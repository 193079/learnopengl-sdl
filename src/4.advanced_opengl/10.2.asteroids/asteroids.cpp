#define STB_IMAGE_IMPLEMENTATION   

#include <SDL.h>
#include <glew.h>
#include <glm.hpp>

#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

#include "learnopengl/filesystem.h"
#include "learnopengl/shader.h"
#include "learnopengl/camera.h"
#include "learnopengl/model.h"

#include <iostream>

// settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

// camera
Camera camera(glm::vec3(0.0f, 0.0f, 55.0f));
float lastX = (float)SCR_WIDTH / 2.0f;
float lastY = (float)SCR_HEIGHT / 2.0f;
bool firstMouse = true;

Uint32 Mouse_Buttons;
int Mouse_x, Mouse_y;

// timing
float deltaTime = 0.0f;	// time between current frame and last frame
float lastFrame = 0.0f;

float TimeChange;

int main(int argc, char* argv[])
{

	// initialize and configure
	// ------------------------------
	SDL_Window* mainwindow;
	SDL_GLContext maincontext;
	SDL_Event event;
	int running = 1;


	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);

	SDL_GL_SetSwapInterval(1);

	mainwindow = SDL_CreateWindow("LearnOpenGL", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SCR_WIDTH, SCR_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);

	if (!mainwindow) {
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

	maincontext = SDL_GL_CreateContext(mainwindow);

	glewExperimental = GL_TRUE;

	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		/* Problem: glewInit failed, something is seriously wrong. */
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
	}
	fprintf(stdout, "Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));

	// configure global opengl state
	// -----------------------------
	glEnable(GL_DEPTH_TEST);

	// build and compile shaders
	// -------------------------
	Shader shader("res/advanced/10.2.instancing.vs", "res/advanced/10.2.instancing.fs");

	// load models
	// -----------
	Model rock("res/objects/rock/rock.obj");
	Model planet("res/objects/planet/planet.obj");

	// generate a large list of semi-random model transformation matrices
	// ------------------------------------------------------------------
	unsigned int amount = 1000;
	glm::mat4* modelMatrices;
	modelMatrices = new glm::mat4[amount];
	srand(TimeChange); // initialize random seed	
	float radius = 50.0;
	float offset = 2.5f;
	for (unsigned int i = 0; i < amount; i++)
	{
		glm::mat4 model = glm::mat4(1.0f);
		// 1. translation: displace along circle with 'radius' in range [-offset, offset]
		float angle = (float)i / (float)amount * 360.0f;
		float displacement = (rand() % (int)(2 * offset * 100)) / 100.0f - offset;
		float x = sin(angle) * radius + displacement;
		displacement = (rand() % (int)(2 * offset * 100)) / 100.0f - offset;
		float y = displacement * 0.4f; // keep height of asteroid field smaller compared to width of x and z
		displacement = (rand() % (int)(2 * offset * 100)) / 100.0f - offset;
		float z = cos(angle) * radius + displacement;
		model = glm::translate(model, glm::vec3(x, y, z));

		// 2. scale: Scale between 0.05 and 0.25f
		float scale = (rand() % 20) / 100.0f + 0.05;
		model = glm::scale(model, glm::vec3(scale));

		// 3. rotation: add random rotation around a (semi)randomly picked rotation axis vector
		float rotAngle = (rand() % 360);
		model = glm::rotate(model, rotAngle, glm::vec3(0.4f, 0.6f, 0.8f));

		// 4. now add to list of matrices
		modelMatrices[i] = model;
	}

	while (running) {

		SDL_Event e;
		while (SDL_PollEvent(&e)) {
			if (e.type == SDL_QUIT) {
				running = false;
			}
			if (e.type == SDL_WINDOWEVENT_CLOSE) {
				running = 0;
				break;
			}

			float cameraSpeed = 2.5 * deltaTime;
			//If a key was pressed
			if (e.type == SDL_KEYDOWN)
			{
				if (e.key.keysym.sym == SDLK_w)
					camera.ProcessKeyboard(FORWARD, deltaTime);
				if (e.key.keysym.sym == SDLK_s)
					camera.ProcessKeyboard(BACKWARD, deltaTime);
				if (e.key.keysym.sym == SDLK_a)
					camera.ProcessKeyboard(LEFT, deltaTime);
				if (e.key.keysym.sym == SDLK_d)
					camera.ProcessKeyboard(RIGHT, deltaTime);
				if (e.key.keysym.sym == SDLK_ESCAPE)
					running = 0;
			}

			SDL_PumpEvents();  // make sure we have the latest mouse state.
			Mouse_Buttons = SDL_GetMouseState(&Mouse_x, &Mouse_y);

			if (firstMouse)
			{
				lastX = (SCR_WIDTH / 2);
				lastY = (SCR_HEIGHT / 2);
				firstMouse = false;
			}

			float xoffset = Mouse_x - lastX;
			float yoffset = lastY - Mouse_y; // reversed since y-coordinates go from bottom to top

			lastX = Mouse_x;
			lastY = Mouse_y;

			camera.ProcessMouseMovement(xoffset, yoffset);

			if (e.type == SDL_MOUSEWHEEL)
			{

				if (e.wheel.y > 0) // scroll up
				{
					camera.ProcessMouseScroll1(4.0f);
				}
				else if (e.wheel.y < 0) // scroll down
				{
					camera.ProcessMouseScroll2(4.0f);
				}
			}
		}

		// per-frame time logic
		// --------------------
		TimeChange = TimeChange + 0.1f;

		float currentFrame = TimeChange;
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		// render
		// ------
		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// configure transformation matrices
		glm::mat4 projection = glm::perspective(glm::radians(45.0f), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 1000.0f);
		glm::mat4 view = camera.GetViewMatrix();;
		shader.use();
		shader.setMat4("projection", projection);
		shader.setMat4("view", view);

		// draw planet
		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(0.0f, -3.0f, 0.0f));
		model = glm::scale(model, glm::vec3(4.0f, 4.0f, 4.0f));
		shader.setMat4("model", model);
		planet.Draw(shader);

		// draw meteorites
		for (unsigned int i = 0; i < amount; i++)
		{
			shader.setMat4("model", modelMatrices[i]);
			rock.Draw(shader);
		}

		SDL_GL_SwapWindow(mainwindow);
	}

	SDL_GL_DeleteContext(maincontext);
	SDL_DestroyWindow(mainwindow);
	SDL_Quit();

	return 0;
}
